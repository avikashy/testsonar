//
//  SampleTests.swift
//  SampleTests
//
//  Created by Avinash Kashyap on 01/02/22.
//

import XCTest
@testable import Sample

class SampleTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testTotal() {
        let total = ViewController().getTotal(a: 10, b: 10)
        XCTAssertTrue(total == 20)
    }

}
